import pandas as pd
import mazdap as mz

import os
from sqlalchemy import create_engine
from sqlalchemy.types import DateTime, Float, Integer, String

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV"
)
conn = mz.ObieeConnector()

max_date = "2020-09-01"
min_date = "2018-01-01"

cx_dump = []
for i in pd.date_range(min_date, max_date, freq="MS"):
    dt = f"{i.year}{i.month:02}"
    cx_df = conn.get_excel(
        "/users/wphyo/CX Response Service",
        skiprows=0,
        filters=mz.obiee.define_filters(
            "comparison",
            "equal",
            '"Response Date"."Calendar Year Month Id"',
            {int(dt): "decimal"},
        ),
    )
    cx_df["Calendar Date"] = cx_df["Calendar Date"].ffill()
    cx_dump.append(cx_df)
    print(f"Finished getting data for : {dt}")


cx_df_final = pd.concat(cx_dump)

dtypes = {}
str_cols = [
    "Dealer Code",
    "Survey ID",
    "Repair Order ID",
    "Survey Status",
    "Mulligan Flag",
    "Influence Flag",
    "Ownership Flag",
    "Carline",
    "VIN Code",
    "Brand",
    "FIRFT",
    "Reason Vehicle not Serviced\Repaired Correctly",
    "Scheduled Appointment New",
    "Scheduled Appointment Old",
    "Service Completed Under One Hour",
    "Service Intent to Influence",
    "Service Survey Mention",
]
for i in cx_df_final.columns:
    if i in ["Calendar Date", "Repair Complete Date"]:
        dtypes[i] = DateTime()
    elif i in str_cols:
        dtypes[i] = String(500)
    else:
        dtypes[i] = Float()


cx_df_final.to_sql(
    "wp_cx360_service_response", engine, if_exists="replace", index=False, dtype=dtypes
)
