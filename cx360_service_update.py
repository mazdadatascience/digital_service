import pandas as pd
import mazdap as mz
import datetime
from dateutil.relativedelta import relativedelta

conn = mz.ObieeConnector()


## get the last 3 months data
max_date_dt = (datetime.date.today() - relativedelta(months=1)).replace(day=1)
min_date_dt = max_date_dt - relativedelta(months=2)
max_date = max_date_dt.strftime("%Y-%m-%d")
min_date = min_date_dt.strftime("%Y-%m-%d")

cx_dump = []
for i in pd.date_range(min_date, max_date, freq="MS"):
    dt = f"{i.year}{i.month:02}"
    cx_df = conn.get_excel(
        "/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/CX Response Service",
        skiprows=0,
        filters=mz.obiee.define_filters(
            "comparison",
            "equal",
            '"Response Date"."Calendar Year Month Id"',
            {int(dt): "decimal"},
        ),
    )
    cx_df["Calendar Date"] = cx_df["Calendar Date"].ffill()
    cx_dump.append(cx_df)
    print(f"Finished getting data for : {dt}")


cx_df_final = pd.concat(cx_dump)


import os
from sqlalchemy import create_engine
from sqlalchemy.types import DateTime, Float, Integer, String

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV"
)

dtypes = {}
str_cols = [
    "Dealer Code",
    "Survey ID",
    "Repair Order ID",
    "Survey Status",
    "Mulligan Flag",
    "Influence Flag",
    "Ownership Flag",
    "Carline",
    "VIN Code",
    "Brand",
    "FIRFT",
    "Reason Vehicle not Serviced\\Repaired Correctly",
    "Scheduled Appointment New",
    "Scheduled Appointment Old",
    "Service Completed Under One Hour",
    "Service Intent to Influence",
    "Service Survey Mention",
]
for i in cx_df_final.columns:
    if i in ["Calendar Date", "Repair Complete Date"]:
        dtypes[i] = DateTime()
    else:
        dtypes[i] = String(500)

## remove the past three months of data...
engine.connect().execute(
    "delete from rpr_stg.wp_cx360_service_response where \"Calendar Date\" >= (date '{}')".format(
        min_date
    )
)

cx_df_final.to_sql(
    "wp_cx360_service_response", engine, if_exists="append", index=False, dtype=dtypes
)
