"""This script uses mazdap to gather all Customer Pay and Warranty ROs
from the 'Vehicle - Repair Order Claim' subject area and writes to Oracle DB.

Time Period: Previous 12 months based on current date
Table(s): WP_DST_RO_RECORDS
"""

import datetime
import os
import pickle

import pandas as pd
from dateutil.relativedelta import relativedelta
from sqlalchemy import create_engine
from sqlalchemy.types import DateTime, Float, Integer, String
import mazdap as mz
from credentials import login


conn = mz.ObieeConnector(login['uid'], login['pwd'])

max_date_dt = (datetime.date.today() - relativedelta(months=1)).replace(day=1)
min_date_dt = max_date_dt - relativedelta(months=11)
max_date = max_date_dt.strftime("%Y-%m-%d")
min_date = min_date_dt.strftime("%Y-%m-%d")

# mid_date_dt = datetime.date.today().replace(day=15)
# mid_date = mid_date_dt.strftime("%Y-%m-%d")

# ro query
query_template = """SELECT
   "Vehicle - Repair Order Claim"."Dealer Hierarchy"."RO Dealer Code" s_5,
   "Vehicle - Repair Order Claim"."Facts"."RO VIN Code",
   "Vehicle - Repair Order Claim"."Dealer Hierarchy"."RO Dealer Name" s_5,
   "Vehicle - Repair Order Claim"."Facts"."Cust Total Amt",
   "Vehicle - Repair Order Claim"."Facts"."RO Claim Code",
   "Vehicle - Repair Order Claim"."Facts"."RO Claim Sequence Number",
   "Vehicle - Repair Order Claim"."Facts"."Service Pay Type",
   "Vehicle - Repair Order Claim"."Facts"."Total Cust Part Amt",
   "Vehicle - Repair Order Claim"."Date - Repair Order Completion"."RO Completion Calendar Date",
   "Vehicle - Repair Order Claim"."Vehicle - Master"."Retail Sale Date"
FROM "Vehicle - Repair Order Claim"
WHERE
(("Dealer Hierarchy"."RO Country Code" = 'US') AND 
("Facts"."RO Count" = 1) AND ("Facts"."Service Pay Type" IN ('W', 'C')) AND 
("Date - Repair Order Completion"."RO Completion Calendar Year Month ID" BETWEEN {min_dt_yrmoid} AND {max_dt_yrmoid}))"""

min_dt_yrmoid = min_date_dt.strftime("%Y%m")
max_dt_yrmoid = max_date_dt.strftime("%Y%m")

ro_cols = [
    "DEALER_CODE",
    "VIN",
    "DLR_NAME",
    "RO_TOT_AMT",
    "RO_NUMBER",
    "RO_CLAIM_SEQ_NUMBER",
    "SERVICE_PAY_TYPE",
    "RO_PART_TOT_AMT",
    "RO_CAL_DATE",
    "RTL_SLS_DATE",
]
ro_df = conn.exec_sql(
    query_template.format(min_dt_yrmoid=min_dt_yrmoid, max_dt_yrmoid=max_dt_yrmoid),
    custom_cols=ro_cols,
)

ro_df["RO_CAL_DATE"] = pd.to_datetime(ro_df["RO_CAL_DATE"])
ro_df["RTL_SLS_DATE"] = pd.to_datetime(ro_df["RTL_SLS_DATE"], errors="coerce")
ro_df[["RO_TOT_AMT", "RO_PART_TOT_AMT"]] = ro_df[
    ["RO_TOT_AMT", "RO_PART_TOT_AMT"]
].astype(float)
ro_df["RO_NUMBER"] = ro_df["RO_NUMBER"].str.lstrip("0")

with open("ro_data.pkl", "wb") as output:  # Overwrites any existing file.
    pickle.dump(ro_df, output, pickle.HIGHEST_PROTOCOL)

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV"
)

dtypes = {}
str_cols = [
    "DEALER_CODE",
    "VIN",
    "DLR_NAME",
    "RO_NUMBER",
    "RO_CLAIM_SEQ_NUMBER",
    "SERVICE_PAY_TYPE",
]

for i in ro_df.columns:
    if i in ["RO_CAL_DATE", "RTL_SLS_DATE"]:
        dtypes[i] = DateTime()
    elif i in str_cols:
        dtypes[i] = String(500)
    else:
        dtypes[i] = Float()

ro_df.to_sql(
    "wp_dst_ro_records", engine, if_exists="replace", index=False, dtype=dtypes
)

