import pandas as pd
import os
import subprocess
import datetime

from sqlalchemy import create_engine

import mazdap as mz

conn = mz.ObieeConnector()

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV",
    max_identifier_length=128,
)


agg_df = pd.read_sql("SELECT * FROM WP_DST_AGG_DEALER", engine)
agg_df.columns = map(str.upper, agg_df.columns)

dlr_query = """SELECT
   "Vehicle - Sales"."Dealer Hierarchy"."Dealer Cd",
   "Vehicle - Sales"."Dealer Hierarchy"."District Code",
   "Vehicle - Sales"."Dealer Detail"."Rgn Cd"
FROM "Vehicle - Sales"
WHERE
("Retail Region"."Retail Country Cd" = 'US')"""

dlr_master = conn.exec_sql(dlr_query, custom_cols=["DEALER_CODE", "DISTRICT", "REGION_CD"])

agg_df = agg_df.merge(dlr_master, on="DEALER_CODE", how="left")
agg_df = agg_df[
    [
        "DEALER_CODE",
        "REGION_CD",
        "DISTRICT",
        "DLR_NAME",
        "VIN",
        "RO_NUMBER",
        "RO_TOT_AMT",
        "RO_PART_TOT_AMT",
        "SERVICE_PAY_TYPE",
        "RO_CAL_DATE",
        "TIME_SENT",
        "TIME_VIEWED",
        "VIEW_COUNT",
        "SERVICE_OVR_STFN",
        "SERVICE_LKL_RETURN",
        "SERVICE_QLY_OSAT",
        "VAL_COS",
        "VID_VIEWED",
        "VID_SENT",
        "SURVEY_MATCH",
        "VENDOR_MATCH"
    ]
]

agg_df.to_csv("agg_data/WP_DST_AGG_DLR.csv", index=False)
print("Task: Writing WP_DST_AGG.csv - Completed.")

sync_cmd = "rclone sync -v agg_data sharepoint:wphyo/digital_service"
with open("logging.txt", "w") as f:
    process = subprocess.Popen(sync_cmd, shell=True, stderr=f, universal_newlines=True)
    while True:
        return_code = process.poll()
        if return_code is not None:
            break
#### SYNC ONEDRIVE ####

conn.logoff()
